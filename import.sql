-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : lun. 25 mai 2020 à 15:01
-- Version du serveur :  5.7.28-0ubuntu0.18.04.4
-- Version de PHP : 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `cocovoit`
--

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `email`, `roles`, `password`, `name_user`, `forename_user`, `phone_user`, `address_user`, `city_user`, `postal_code_user`, `car_model_user`) VALUES
(9, 'mat@mat.fr', '[\"ROLE_ADMIN\"]', '$argon2id$v=19$m=65536,t=4,p=1$2s9YTfNH+Q6eiN8mBllkcg$+EGJq9Uyhp6fW1+z+EhZnPO2e1WlTd7vFgb79x6w5Us', 'matthias', 'matthias', '0606060606', 'rue', 'ville', 0, 'clio'),
(11, 'test@test.fr', '[\"ROLE_USER\"]', '$argon2id$v=19$m=65536,t=4,p=1$o1AKIXWm/Mzcx6DpZmBhwg$o90OHPksPe0ocduDi7MC7zdpYrN7WwkNs25+kAgzLDg', 'azezrtyuiop', 'zertyuiop', '243567', 'zeretyuio', 'zretyuioi', 23546, 'zerty'),
(12, 'jean.dupont@gmail.com', '[\"ROLE_USER\"]', '$argon2id$v=19$m=65536,t=4,p=1$KHH0IuAFVilPXgQqwP6j+w$byM7XGZFj+FhIfgS07nM10PhSAeuDeFBG36ap5AYm5Y', 'Dupont', 'Jean', '0606060606', '1 rue de paris', 'Nantes', 44000, 'Tesla Model S'),
(13, 'lambda@gmail.com', '[\"ROLE_USER\"]', '$argon2id$v=19$m=65536,t=4,p=1$7kP2rXcJdvBAh/xrbXjnMg$f/qvgSiE/Ir59JX3hlIo1O1JMNVDOfC1x6SEf+mZWBQ', 'Smith', 'John', '0606060606', '12 avenue des inconnus', 'Los Angeles', 33333, 'Mustang');

--
-- Déchargement des données de la table `annonce`
--

INSERT INTO `annonce` (`id`, `id_user_id`, `nb_places`, `animaux_autorisees`, `fumeurs_autorises`, `adresse_depart`, `ville_depart`, `adresse_arrivee`, `ville_arrivee`, `date_heure_depart`) VALUES
(3, 12, 4, 1, 0, '1 rue de paris', 'Nantes', '58 avenue de la revolution', 'Lyon', '2020-05-07 15:30:00'),
(5, 13, 4, 0, 1, '2 rue de la paie', 'Angers', '6 rue de la route', 'Paries', '2015-08-03 00:00:00'),
(6, 13, 4, 1, 1, '23 avenue de la paie', 'Angers', '3 rue des hirondelles', 'Brest', '2015-09-01 00:00:00'),
(7, 13, 4, 1, 1, '2 avenue des hirondelles', 'Angers', '43bis rue du moulin', 'Toulouse', '2020-11-01 00:00:00'),
(8, 13, 4, 1, 1, '3 rue du moulin', 'Angers', '6 rue de l\'hirondelle', 'Metz', '2020-07-01 00:00:00'),
(9, 12, 4, 1, 1, '4 rue de la faille', 'Nantes', '4 rue des ponts', 'Angers', '2015-01-01 00:00:00'),
(10, 12, 4, 1, 1, '4 rue du moulin', 'Nantes', '6 rue des ponts', 'Angers', '2020-07-01 00:00:00'),
(11, 12, 4, 1, 1, '7 rue de Jean Luc', 'Nanterres', '8 rue de l\'apogée', 'Angers', '2020-12-01 00:00:00');

--
-- Déchargement des données de la table `commenter`
--

INSERT INTO `commenter` (`id`, `id_annonce_id`, `id_user_id`, `commentaire`, `note`) VALUES
(1, 5, 12, 'Trajet fait en toute sécurité, conducteur amiable', 5),
(2, 9, 13, 'Trajet agréable', 4);

--
-- Déchargement des données de la table `reserver`
--

INSERT INTO `reserver` (`id`, `id_annonce_id`, `id_user_id`, `nb_places`) VALUES
(3, 5, 12, 2),
(4, 6, 12, 2),
(5, 9, 13, 1),
(7, 3, 13, 3);

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
