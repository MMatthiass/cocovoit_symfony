<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AnnonceRepository")
 */
class Annonce
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="annonces")
     */
    private $idUser;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbPlaces;

    /**
     * @ORM\Column(type="boolean")
     */
    private $animauxAutorisees;

    /**
     * @ORM\Column(type="boolean")
     */
    private $fumeursAutorises;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $adresseDepart;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $villeDepart;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $adresseArrivee;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $villeArrivee;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateHeureDepart;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Reserver", mappedBy="idAnnonce", orphanRemoval=true)
     */
    private $reservations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Commenter", mappedBy="idAnnonce")
     */
    private $commenters;

    public function __construct()
    {
        $this->reservations = new ArrayCollection();
        $this->commenters = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdUser(): ?User
    {
        return $this->idUser;
    }

    public function setIdUser(?User $idUser): self
    {
        $this->idUser = $idUser;

        return $this;
    }

    public function getNbPlaces(): ?int
    {
        return $this->nbPlaces;
    }

    public function setNbPlaces(int $nbPlaces): self
    {
        $this->nbPlaces = $nbPlaces;

        return $this;
    }

    public function getAnimauxAutorisees(): ?bool
    {
        return $this->animauxAutorisees;
    }

    public function setAnimauxAutorisees(bool $animauxAutorisees): self
    {
        $this->animauxAutorisees = $animauxAutorisees;

        return $this;
    }

    public function getFumeursAutorises(): ?bool
    {
        return $this->fumeursAutorises;
    }

    public function setFumeursAutorises(bool $fumeursAutorises): self
    {
        $this->fumeursAutorises = $fumeursAutorises;

        return $this;
    }

    public function getAdresseDepart(): ?string
    {
        return $this->adresseDepart;
    }

    public function setAdresseDepart(string $adresseDepart): self
    {
        $this->adresseDepart = $adresseDepart;

        return $this;
    }

    public function getVilleDepart(): ?string
    {
        return $this->villeDepart;
    }

    public function setVilleDepart(string $villeDepart): self
    {
        $this->villeDepart = $villeDepart;

        return $this;
    }

    public function getAdresseArrivee(): ?string
    {
        return $this->adresseArrivee;
    }

    public function setAdresseArrivee(string $adresseArrivee): self
    {
        $this->adresseArrivee = $adresseArrivee;

        return $this;
    }

    public function getVilleArrivee(): ?string
    {
        return $this->villeArrivee;
    }

    public function setVilleArrivee(string $villeArrivee): self
    {
        $this->villeArrivee = $villeArrivee;

        return $this;
    }

    public function getDateHeureDepart(): ?\DateTimeInterface
    {
        return $this->dateHeureDepart;
    }

    public function setDateHeureDepart(\DateTimeInterface $dateHeureDepart): self
    {
        $this->dateHeureDepart = $dateHeureDepart;

        return $this;
    }

    /**
     * @return Collection|Reserver[]
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    public function addReservation(Reserver $reservation): self
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations[] = $reservation;
            $reservation->setIdAnnonce($this);
        }

        return $this;
    }

    public function removeReservation(Reserver $reservation): self
    {
        if ($this->reservations->contains($reservation)) {
            $this->reservations->removeElement($reservation);
            // set the owning side to null (unless already changed)
            if ($reservation->getIdAnnonce() === $this) {
                $reservation->setIdAnnonce(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Commenter[]
     */
    public function getCommenters(): Collection
    {
        return $this->commenters;
    }

    public function addCommenter(Commenter $commenter): self
    {
        if (!$this->commenters->contains($commenter)) {
            $this->commenters[] = $commenter;
            $commenter->setIdAnnonce($this);
        }

        return $this;
    }

    public function removeCommenter(Commenter $commenter): self
    {
        if ($this->commenters->contains($commenter)) {
            $this->commenters->removeElement($commenter);
            // set the owning side to null (unless already changed)
            if ($commenter->getIdAnnonce() === $this) {
                $commenter->setIdAnnonce(null);
            }
        }

        return $this;
    }
}
