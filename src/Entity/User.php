<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $nameUser;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $forenameUser;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $phoneUser;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $addressUser;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $cityUser;

    /**
     * @ORM\Column(type="integer")
     */
    private $postalCodeUser;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $carModelUser;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Annonce", mappedBy="idUser")
     */
    private $annonces;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Reserver", mappedBy="idUser")
     */
    private $reservations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Commenter", mappedBy="idUser")
     */
    private $commenters;

    public function __construct()
    {
        $this->annonces = new ArrayCollection();
        $this->reservations = new ArrayCollection();
        $this->commenters = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getNameUser(): ?string
    {
        return $this->nameUser;
    }

    public function setNameUser(string $nameUser): self
    {
        $this->nameUser = $nameUser;

        return $this;
    }

    public function getForenameUser(): ?string
    {
        return $this->forenameUser;
    }

    public function setForenameUser(string $forenameUser): self
    {
        $this->forenameUser = $forenameUser;

        return $this;
    }

    public function getPhoneUser(): ?string
    {
        return $this->phoneUser;
    }

    public function setPhoneUser(string $phoneUser): self
    {
        $this->phoneUser = $phoneUser;

        return $this;
    }

    public function getAddressUser(): ?string
    {
        return $this->addressUser;
    }

    public function setAddressUser(string $addressUser): self
    {
        $this->addressUser = $addressUser;

        return $this;
    }

    public function getCityUser(): ?string
    {
        return $this->cityUser;
    }

    public function setCityUser(string $cityUser): self
    {
        $this->cityUser = $cityUser;

        return $this;
    }

    public function getPostalCodeUser(): ?int
    {
        return $this->postalCodeUser;
    }

    public function setPostalCodeUser(int $postalCodeUser): self
    {
        $this->postalCodeUser = $postalCodeUser;

        return $this;
    }

    public function getCarModelUser(): ?string
    {
        return $this->carModelUser;
    }

    public function setCarModelUser(string $carModelUser): self
    {
        $this->carModelUser = $carModelUser;

        return $this;
    }

    /**
     * @return Collection|Annonce[]
     */
    public function getAnnonces(): Collection
    {
        return $this->annonces;
    }

    public function addAnnonce(Annonce $annonce): self
    {
        if (!$this->annonces->contains($annonce)) {
            $this->annonces[] = $annonce;
            $annonces->setIdUser($this);
        }

        return $this;
    }

    public function removeAnnonces(Annonce $annonce): self
    {
        if ($this->annonces->contains($annonce)) {
            $this->annonces->removeElement($annonce);
            // set the owning side to null (unless already changed)
            if ($annonce->getIdUser() === $this) {
                $annonce->setIdUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Reserver[]
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    public function addReservation(Reserver $reservation): self
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations[] = $reservation;
            $reservation->setIdUser($this);
        }

        return $this;
    }

    public function removeReservation(Reserver $reservation): self
    {
        if ($this->reservations->contains($reservation)) {
            $this->reservations->removeElement($reservation);
            // set the owning side to null (unless already changed)
            if ($reservation->getIdUser() === $this) {
                $reservation->setIdUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Commenter[]
     */
    public function getCommenters(): Collection
    {
        return $this->commenters;
    }

    public function addCommenter(Commenter $commenter): self
    {
        if (!$this->commenters->contains($commenter)) {
            $this->commenters[] = $commenter;
            $commenter->setIdUser($this);
        }

        return $this;
    }

    public function removeCommenter(Commenter $commenter): self
    {
        if ($this->commenters->contains($commenter)) {
            $this->commenters->removeElement($commenter);
            // set the owning side to null (unless already changed)
            if ($commenter->getIdUser() === $this) {
                $commenter->setIdUser(null);
            }
        }

        return $this;
    }
}
