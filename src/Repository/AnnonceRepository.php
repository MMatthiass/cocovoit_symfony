<?php

namespace App\Repository;

use App\Entity\Annonce;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Annonce|null find($id, $lockMode = null, $lockVersion = null)
 * @method Annonce|null findOneBy(array $criteria, array $orderBy = null)
 * @method Annonce[]    findAll()
 * @method Annonce[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnnonceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Annonce::class);
    }

    public function findAllIdsAnnonces($id) : array
    {
        
        return $this->createQueryBuilder('a')
        ->select('a.id')
        ->Where('a.idUser = :id')
        ->setParameter('id', $id)
        ->getQuery()
        ->getResult();
    }

    // /**
    //  * @return Annonce[] Returns an array of Annonce objects
    //  */
    public function findAllAnnonces($id, $date, $idsReservation)
    {
        $qb =$this->createQueryBuilder('a');
        if (isset($idsReservation) && !empty($idsReservation)) {
            $qb->andWhere('a.id NOT IN (:idsResa)');
            $qb->setParameter('idsResa', $idsReservation);           
        }   
        $qb ->andWhere('a.idUser != :val');
        $qb->setParameter('val', $id);
        $qb->andWhere('a.dateHeureDepart >= :date');
        $qb->setParameter('date', $date);
        $qb->orderBy('a.dateHeureDepart', 'ASC');
            
        return $qb->getQuery()->getResult();
    }
    
    public function findAllAnnoncesWithFilter($id, $data, $idsReservation)
    {
        $qb = $this->createQueryBuilder('a');
        if (isset($idsReservation) && !empty($idsReservation)) {
            $qb->andWhere('a.id NOT IN (:idsResa)');
            $qb->setParameter('idsResa', $idsReservation);           
        }   
        $qb->andWhere('a.idUser != :val');
        $qb->setParameter('val', $id);
        if (isset($data['dateHeureDepart']) && !empty($data['dateHeureDepart'])) {
                $qb->andWhere('a.dateHeureDepart >= :date ');
                $qb->setParameter('date', $data['dateHeureDepart']);            
        }
        if (isset($data['villeDepart']) && !empty($data['villeDepart'])) {
            $qb->andWhere('LOWER(a.villeDepart) LIKE LOWER(:ville)');
            $qb->setParameter('ville', $data['villeDepart']);            
        }
        if (isset($data['villeArrivee']) && !empty($data['villeArrivee'])) {
            $qb->andWhere('LOWER(a.villeArrivee) LIKE LOWER(:ville2)');
            $qb->setParameter('ville2', $data['villeArrivee']);            
        }
        if (isset($data['animauxAutorisees']) && !empty($data['animauxAutorisees'])) {
            if ($data['animauxAutorisees'] == 1) {
                $qb->andWhere('a.animauxAutorisees = 1');
            }
            elseif ($data['animauxAutorisees'] == 0) {
                $qb->andWhere('a.animauxAutorisees = 0');
            }
        }
        if (isset($data['fumeursAutorises']) && !empty($data['fumeursAutorises'])) {
            if ($data['fumeursAutorises'] == 1) {
                $qb->andWhere('a.fumeursAutorises = 1');
            }
            elseif ($data['fumeursAutorises'] == 0) {
                $qb->andWhere('a.fumeursAutorises = 0');
            }
        }
        $qb->orderBy('a.dateHeureDepart', 'ASC');
        return $qb->getQuery()->getResult();
    }

    public function findTravelWithFilter($id, $data)
    {
        $qb = $this->createQueryBuilder('a');
        $qb->andWhere('a.idUser = :val');
        $qb->setParameter('val', $id);
        if (isset($data['dateHeureDepart']) && !empty($data['dateHeureDepart'])) {
                $qb->andWhere('a.dateHeureDepart >= :date ');
                $qb->setParameter('date', $data['dateHeureDepart']);            
        }
        if (isset($data['villeDepart']) && !empty($data['villeDepart'])) {
            $qb->andWhere('LOWER(a.villeDepart) LIKE LOWER(:ville)');
            $qb->setParameter('ville', $data['villeDepart']);            
        }
        if (isset($data['villeArrivee']) && !empty($data['villeArrivee'])) {
            $qb->andWhere('LOWER(a.villeArrivee) LIKE LOWER(:ville2)');
            $qb->setParameter('ville2', $data['villeArrivee']);            
        }
        if (isset($data['animauxAutorisees']) && !empty($data['animauxAutorisees'])) {
            if ($data['animauxAutorisees'] == 1) {
                $qb->andWhere('a.animauxAutorisees = 1');
            }
            elseif ($data['animauxAutorisees'] == 0) {
                $qb->andWhere('a.animauxAutorisees = 0');
            }
        }
        if (isset($data['fumeursAutorises']) && !empty($data['fumeursAutorises'])) {
            if ($data['fumeursAutorises'] == 1) {
                $qb->andWhere('a.fumeursAutorises = 1');
            }
            elseif ($data['fumeursAutorises'] == 0) {
                $qb->andWhere('a.fumeursAutorises = 0');
            }
        }
        $qb->orderBy('a.dateHeureDepart', 'ASC');
        return $qb->getQuery()->getResult();
    }

    public function findAllTravelWithFilter($data)
    {
        $qb = $this->createQueryBuilder('a');
        if (isset($data['dateHeureDepart']) && !empty($data['dateHeureDepart'])) {
                $qb->andWhere('a.dateHeureDepart >= :date ');
                $qb->setParameter('date', $data['dateHeureDepart']);            
        }
        if (isset($data['villeDepart']) && !empty($data['villeDepart'])) {
            $qb->andWhere('LOWER(a.villeDepart) LIKE LOWER(:ville)');
            $qb->setParameter('ville', $data['villeDepart']);            
        }
        if (isset($data['villeArrivee']) && !empty($data['villeArrivee'])) {
            $qb->andWhere('LOWER(a.villeArrivee) LIKE LOWER(:ville2)');
            $qb->setParameter('ville2', $data['villeArrivee']);            
        }
        if (isset($data['animauxAutorisees']) && !empty($data['animauxAutorisees'])) {
            if ($data['animauxAutorisees'] == 1) {
                $qb->andWhere('a.animauxAutorisees = 1');
            }
            elseif ($data['animauxAutorisees'] == 0) {
                $qb->andWhere('a.animauxAutorisees = 0');
            }
        }
        if (isset($data['fumeursAutorises']) && !empty($data['fumeursAutorises'])) {
            if ($data['fumeursAutorises'] == 1) {
                $qb->andWhere('a.fumeursAutorises = 1');
            }
            elseif ($data['fumeursAutorises'] == 0) {
                $qb->andWhere('a.fumeursAutorises = 0');
            }
        }
        $qb->orderBy('a.dateHeureDepart', 'ASC');
        return $qb->getQuery()->getResult();
    }
    /*
    public function findOneBySomeField($value): ?Annonce
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
