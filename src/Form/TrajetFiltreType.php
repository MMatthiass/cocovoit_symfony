<?php

namespace App\Form;

use App\Entity\Annonce;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class TrajetFiltreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateHeureDepart', DateTimeType::class, [
                'required' => false,
                'label' => 'ad.start_time',
                'data' => new \DateTime(),
            ])
            ->add('villeDepart', TextType::class, [
                'required' => false,
                'label' => 'ad.start_city',
                'attr' => [
                    'placeholder' => 'ad.start_city'
                ]
            ])
            ->add('villeArrivee', TextType::class, [
                'required' => false,
                'label' => 'ad.end_city',
                'attr' => [
                    'placeholder' => 'ad.end_city'
                ]
            ])
            ->add('fumeursAutorises',ChoiceType::class,[
                'choices' => ['yes'=>1, 'no' => 0, 'both' => null],
                'label' => 'ad.smoking'
            ])
            ->add('animauxAutorisees',ChoiceType::class,[
                'choices' => ['yes'=>1, 'no' => 0, 'both' => null],
                'label' => 'ad.animals'
            ])
            ->add('Recherche', SubmitType::class, [
                'label' => 'search'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
    }
}
