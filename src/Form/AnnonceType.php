<?php

namespace App\Form;

use App\Entity\Annonce;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class AnnonceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nbPlaces', IntegerType::class, [
                'required' => true,
                'label' => 'ad.number_place'
            ])
            ->add('animauxAutorisees',ChoiceType::class,[
                'choices' => ['yes'=>1, 'no' => 0,],
                'label' => 'ad.animals'
            ])
            ->add('fumeursAutorises',ChoiceType::class,[
                'choices' => ['yes'=>1, 'no' => 0,],
                'label' => 'ad.smoking'
                ])
            ->add('adresseDepart',TextType::class, [
                'required' => true,
                'label' => 'ad.start_address'
            ])
            ->add('villeDepart',TextType::class, [
                'required' => true,
                'label' => 'ad.start_city'
            ])
            ->add('adresseArrivee',TextType::class, [
                'required' => true,
                'label' => 'ad.end_address'
            ])
            ->add('villeArrivee',TextType::class, [
                'required' => true,
                'label' => 'ad.end_city'
            ])
            ->add('dateHeureDepart',DateTimeType::class, [
                'required' => true,
                'label' => 'ad.start_time'
            ])
            ->add('reset', ResetType::class, [
                'label' => 'reset'
            ])
            ->add('validate', SubmitType::class, [
                'label' => 'validate'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Annonce::class,
        ]);
    }
}
