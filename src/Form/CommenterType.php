<?php

namespace App\Form;

use App\Entity\Commenter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CommenterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('commentaire', TextType::class,[
                'label' => 'comment'
            ])
            ->add('note',ChoiceType::class,[
                'choices' => ['1'=>1, '2' => 2,'3' => 3,'4' => 4,'5' => 5,],
                'label' => 'rating'
                ])
            ->add('Valider', SubmitType::class, [
                'label' => 'validate'
            ])
        ;
    }
}
