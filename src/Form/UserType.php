<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'required' => true,
                'label' => 'user.email'
            ])
            ->add('password', PasswordType::class, [
                'label' => 'password',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Merci d\'entrer un mot de passe',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Your password should be at least {{ limit }} characters',
                        'max' => 4096,
                    ]),
                ],
            ])
            ->add('nameUser', TextType::class, [
                'required' => true,
                'label' => 'user.name'
            ])
            ->add('forenameUser', TextType::class, [
                'required' => true,
                'label' => 'user.firstname'
            ])
            ->add('phoneUser', TelType::class, [
                'required' => true,
                'label' => 'user.phone'
            ])
            ->add('addressUser', TextType::class, [
                'required' => true,
                'label' => 'user.address'
            ])
            ->add('cityUser', TextType::class, [
                'required' => true,
                'label' => 'user.city'
            ])
            ->add('postalCodeUser', NumberType::class, [
                'required' => true,
                'label' => 'user.postal_code'
            ])
            ->add('carModelUser', TextType::class, [
                'required' => true,
                'label' => 'user.vehicle_model'
            ])
            ->add('reset', ResetType::class, [
                'label' => 'reset'
            ])
            ->add('validate', SubmitType::class, [
                'label' => 'validate'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
