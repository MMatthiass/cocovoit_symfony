<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Annonce;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\AnnonceType;
use App\Form\TrajetFiltreType;
use App\Entity\Reserver;

class AnnonceController extends AbstractController
{

    public function index()
    {
        return $this->redirectToRoute('annonce.list');
    }

    /**
     * Afficher la liste des annonces
     * @Route("{_locale}/admin/trajet", name="trajet.listAdmin")
     *
     */
    public function listAdmin(Request $request)
    {
        $form = $this->createForm(TrajetFiltreType::class);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $data = $form->getData();
            $annonces = $this->getDoctrine()->getRepository(Annonce::class)->findAllTravelWithFilter($data);
            return $this->render('annonce/listTrajet.html.twig',[
                'annonces' => $annonces,
                'form' => $form->createView()
            ]);
        }
        $annonces = $this->getDoctrine()->getRepository(Annonce::class)->findAll();
        return $this->render('annonce/listTrajet.html.twig',[
            'annonces' => $annonces,
            'form' => $form->createView()
        ]);
    }

    /**
     * Afficher la liste des annonces d'un utilisateur
     * @Route("{_locale}/trajet", name="trajet.list")
     *
     */
    public function list(Request $request)
    {
        $form = $this->createForm(TrajetFiltreType::class);
        $form->handleRequest($request);

        $idUser = $this->getUser();

        if($form->isSubmitted() && $form->isValid()){
            $data = $form->getData();
            $annonces = $this->getDoctrine()->getRepository(Annonce::class)->findTravelWithFilter($idUser, $data);
            return $this->render('annonce/listTrajet.html.twig',[
                'annonces' => $annonces,
                'form' => $form->createView()
            ]);
        }
        $annonces = $this->getDoctrine()->getRepository(Annonce::class)->findBy(['idUser' => $idUser]);
        return $this->render('annonce/listTrajet.html.twig',[
            'annonces' => $annonces,
            'form' => $form->createView()
        ]);
    }

    /**
     * Afficher la liste des annonces
     * @Route("{_locale}/annonce", name="annonce.list")
     *
     */
    public function listAll(EntityManagerInterface $em, Request $request)
    {
        $form = $this->createForm(TrajetFiltreType::class);
        $form->handleRequest($request);
        $idsReservation = [];
        if ($this->getUser() != null) {
            $idUser = $this->getUser();
            $idsRes = $this->getDoctrine()->getRepository(Reserver::class)->findIdsReservation($idUser);
            foreach ($idsRes as $key => $value) {
                $idsReservation[$key] = $value[1];
            }
        }
        else {
            $idUser = 0;   
        }
        
        if($form->isSubmitted() && $form->isValid()){
            $data = $form->getData();
            $annonces = $this->getDoctrine()->getRepository(Annonce::class)->findAllAnnoncesWithFilter($idUser, $data, $idsReservation);
            return $this->render('annonce/listAnnonce.html.twig',[
                'annonces' => $annonces,
                'form' => $form->createView()
            ]);
        }
        $annonces = $this->getDoctrine()->getRepository(Annonce::class)->findAllAnnonces($idUser, date('Y-m-d h:m:s'), $idsReservation);
        return $this->render('annonce/listAnnonce.html.twig',[
            'annonces' => $annonces,
            'form' => $form->createView()
        ]);
    }

    /**
     * Supprimer une annonce
     * @Route("{_locale}/annonce/suppr/{id}", name="annonce.delete")
     *
     */
    public function delete($id, EntityManagerInterface $em)
    {
        $annonce = $this->getDoctrine()->getRepository(Annonce::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($annonce);
        $em->flush();
        
        return $this->redirectToRoute('trajet.list');
    }

    /**
     * Creer une annonce
     * @Route("{_locale}/annonce/creer", name="annonce.create")
     * @param Request $request
     * @param EntityManagerInterface $em
     */
    public function create(Request $request, EntityManagerInterface $em)
    {
        $annonce = new Annonce();
        $form = $this->createForm(AnnonceType::class, $annonce);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $user = $this->getUser();
            $annonce->setIdUser($user);
            $em->persist($annonce);
            $em->flush();
            return $this->redirectToRoute('annonce.list');
        }
        return $this->render('annonce/create.html.twig',[
            'form' => $form->createView(), 10
        ]);
    }
    /**
     * Copier une annonce
     * @Route("{_locale}/annonce/copier/{id}", name="annonce.copy")
     * @param Request $request
     * @param EntityManagerInterface $em
     */
    public function copy($id, EntityManagerInterface $em)
    {
        $old = $this->getDoctrine()->getRepository(Annonce::class)->find($id);
        $annonce = new Annonce();
        $user = $this->getUser();
        $annonce->setIdUSer($old->getIdUser($user));
        $annonce->setNbPlaces($old->getNbPlaces());
        $annonce->setAnimauxAutorisees($old->getAnimauxAutorisees());
        $annonce->setFumeursAutorises($old->getFumeursAutorises());
        $annonce->setAdresseDepart($old->getAdresseDepart());
        $annonce->setAdresseArrivee($old->getAdresseArrivee());
        $annonce->setVilleDepart($old->getVilleDepart());
        $annonce->setVilleArrivee($old->getVilleArrivee());
        $annonce->setDateHeureDepart($old->getDateHeureDepart());
        $em->persist($annonce);
        $em->flush();
        return $this->redirectToRoute('annonce.list');
    }
    

    /**
     * Modifier une annonce
     * @Route("{_locale}/annonce/modifier/{id}", name="annonce.modify")
     * @param Request $request
     * @param EntityManagerInterface $em
     */
    public function modify($id, Request $request, EntityManagerInterface $em)
    {
        $annonce = $this->getDoctrine()->getRepository(Annonce::class)->find($id);
        $form = $this->createForm(AnnonceType::class, $annonce);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $em->persist($annonce);
            $em->flush();
            return $this->redirectToRoute('annonce.list');
        }
        return $this->render('annonce/create.html.twig',[
            'form' => $form->createView(), 10
        ]);
    }
}
