<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Reserver;
use App\Entity\Annonce;
use App\Form\ReserverType;
class ReserverController extends AbstractController
{
    /**
     * @Route("/reserver1", name="reserver")
     */
    public function index()
    {
        return $this->render('reserver/index.html.twig', [
            'controller_name' => 'ReserverController',
        ]);
    }

    /**
     * Afficher la liste des reservations
     * @Route("{_locale}/reserver", name="reserver.list")
     *
     */
    public function listReservation(){
        $reservations = $this->getDoctrine()->getRepository(Reserver::class)->findBy(['idUser' => $this->getUser()]);

        return $this->render('reserver/list.html.twig',[
            'reservations' => $reservations,
        ]);
    }

    /**
     * Supprimer une réservation
     * @Route("{_locale}/reserver/delete/{id}", name="reserver.delete")
     * @param Request $request
     * @param EntityManagerInterface $em
     */
    public function delete($id, EntityManagerInterface $em)
    {
        $reservation = $this->getDoctrine()->getRepository(Reserver::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($reservation);
        $em->flush();
        
        return $this->listReservation();
    }

     /**
     * Créer une reservation
     * @Route("{_locale}/reserver/creer/{id}", name="reserver.create")
     * @param Request $request
     * @param EntityManagerInterface $em
     */
    public function creerReservation($id, Request $request, EntityManagerInterface $em)
    {
        $reserver = new Reserver();
        $form = $this->createForm(ReserverType::class, $reserver);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $reserver->setIdAnnonce($this->getDoctrine()->getRepository(Annonce::class)->find($id));
            $user = $this->getUser();
            $reserver->setIdUser($user);
            $em->persist($reserver);    
            $em->flush();
            return $this->redirectToRoute('reserver.list');
        }
        return $this->render('reserver/create.html.twig',[
            'form' => $form->createView()
        ]);
    }

        /**
     * Modifier une réservation
     * @Route("{_locale}/reserver/modifier/{id}", name="reserver.modify")
     * @param Request $request
     * @param EntityManagerInterface $em
     */
    public function modify($id, Request $request, EntityManagerInterface $em)
    {
        $reservation = $this->getDoctrine()->getRepository(Reserver::class)->find($id);
        $form = $this->createForm(ReserverType::class, $reservation);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $em->persist($reservation);
            $em->flush();
            return $this->redirectToRoute('reserver.list');
        }
        return $this->render('reserver/create.html.twig',[
            'form' => $form->createView(), 10
        ]);
    }
}
