<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Commenter;
use App\Form\CommenterType;
use App\Entity\Annonce;


class CommenterController extends AbstractController
{
    /**
     * @Route("{_locale}/commenter", name="commenter")
     */
    public function index()
    {
        return $this->render('commenter/index.html.twig', [
            'controller_name' => 'CommenterController',
        ]);
    }

    /**
     * Afficher la liste des commentaires recus
     * @Route("{_locale}/commentairesrecus", name="commenter.listRecu")
     *
     */
    public function listRecu(){
        $idUser = $this->getUser();
        $idsAnn = $this->getDoctrine()->getRepository(Annonce::class)->findAllIdsAnnonces($idUser);
        
        $commentaires = [];
        foreach ($idsAnn as $key => $value) {
            $commentaires += $this->getDoctrine()->getRepository(Commenter::class)->findBy(['idAnnonce' => $value]);
        }

        return $this->render('commenter/listRecu.html.twig',[
            'commentaires' => $commentaires,
        ]);
    }

    /**
     * Afficher la liste des commentaires recus
     * @Route("{_locale}/commentaireslisteannonce/{id}", name="commenter.listAnnonce")
     *
     */
    public function listAnnonce($id){
        $idUser = $id;
        $idsAnn = $this->getDoctrine()->getRepository(Annonce::class)->findAllIdsAnnonces($idUser);
        
        $commentaires = [];
        foreach ($idsAnn as $key => $value) {
            $commentaires += $this->getDoctrine()->getRepository(Commenter::class)->findBy(['idAnnonce' => $value]);
        }

        return $this->render('commenter/listRecu.html.twig',[
            'commentaires' => $commentaires,
        ]);
    }

    /**
     * Afficher la liste des commentaires
     * @Route("{_locale}/commentaires", name="commenter.list")
     *
     */
    public function list(){
        $commentaires = $this->getDoctrine()->getRepository(Commenter::class)->findBy(['idUser' => $this->getUser()]);

        return $this->render('commenter/list.html.twig',[
            'commentaires' => $commentaires,
        ]);
    }

    /**
     * Créer un commentaire
     * @Route("{_locale}/commenter/creer/{id}", name="commenter.create")
     * @param Request $request
     * @param EntityManagerInterface $em
     */
    public function create($id, Request $request, EntityManagerInterface $em)
    {
        $commenter = new Commenter();
        $form = $this->createForm(CommenterType::class, $commenter);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $commenter->setIdAnnonce($this->getDoctrine()->getRepository(Annonce::class)->find($id));
            $user = $this->getUser();
            $commenter->setIdUser($user);
            $em->persist($commenter);    
            $em->flush();
            return $this->redirectToRoute('commenter.list');
        }
        return $this->render('commenter/create.html.twig',[
            'form' => $form->createView()
        ]);
    }


    /**
     * Supprimer un commentaire
     * @Route("{_locale}/commenter/delete/{id}", name="commenter.delete")
     * @param Request $request
     * @param EntityManagerInterface $em
     */
    public function delete($id, EntityManagerInterface $em)
    {
        $commentaire = $this->getDoctrine()->getRepository(Commenter::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($commentaire);
        $em->flush();
        
        return $this->list();
    }
        /**
     * Modifier une réservation
     * @Route("{_locale}/commenter/modifier/{id}", name="commenter.modify")
     * @param Request $request
     * @param EntityManagerInterface $em
     */
    public function modify($id, Request $request, EntityManagerInterface $em)
    {
        $commentaire = $this->getDoctrine()->getRepository(Commenter::class)->find($id);
        $form = $this->createForm(CommenterType::class, $commentaire);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $em->persist($commentaire);
            $em->flush();
            return $this->redirectToRoute('commenter.list');
        }
        return $this->render('commenter/create.html.twig',[
            'form' => $form->createView(), 10
        ]);
    }
}
