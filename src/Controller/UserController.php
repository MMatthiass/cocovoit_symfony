<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\UserType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Cookie;

class UserController extends AbstractController
{
    /**
     * @Route("/theme/{theme}", name="theme")
     */
    public function theme(Request $request, $theme)
    {
        $response = $this->redirectToRoute('annonce.list');
        $response->headers->setcookie(new Cookie('user_theme', $theme));
        return $response;
    }
    /**
     * @Route("{_locale}/user", name="user")
     */
    public function index()
    {
        return $this->render('user/index.html.twig', [
            'controller_name' => 'UserController',
        ]);
    }

    /**
     * Afficher la liste des utilisateurs
     * @Route("{_locale}/admin/user/list", name="user.list")
     *
     */
    public function list()
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();
        return $this->render('user/list.html.twig',[
            'users' => $users,
        ]);
    }

    /**
     * Supprimer un utilisateur
     * @Route("{_locale}/admin/user/suppr/{id}", name="user.delete")
     *
     */
    public function delete($id, EntityManagerInterface $em)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();
        
        return $this->list();
    }

    /**
     * Mettre le role admin
     * @Route("{_locale}/admin/user/set_admin/{id}", name="user.setAdmin")
     *
     */
    public function setAdmin($id, EntityManagerInterface $em)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        $user->setRoles(array('ROLE_ADMIN'));
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        
        return $this->list();
    }

    /**
     * Crée un utilisateur
     *
     * @Route("{_locale}/user/create", name="user.create")
     */
    public function create(Request $request, UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $em)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
        
            $user->setRoles(array('ROLE_USER'));
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('password')->getData()
                )
            );
            $em->persist($user);
            $em->flush();
        }

        return $this->render('user/create.html.twig', [
            'userForm' => $form->createView()
        ]);
    }

    /**
     * Modifier un utilisateur
     *
     * @Route("{_locale}/user/modifier", name="user.modify")
     */
    public function modify(Request $request, UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $em)
    {
        $user = $this->getUser();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){

            $em->persist($user);
            $em->flush();
        }

        return $this->render('user/create.html.twig', [
            'userForm' => $form->createView()
        ]);
    }
}
