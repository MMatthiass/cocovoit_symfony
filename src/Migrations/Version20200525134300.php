<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200525134300 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE reserver (id INT AUTO_INCREMENT NOT NULL, id_annonce_id INT NOT NULL, id_user_id INT NOT NULL, nb_places INT NOT NULL, INDEX IDX_B9765E932D8F2BF8 (id_annonce_id), INDEX IDX_B9765E9379F37AE5 (id_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, name_user VARCHAR(30) NOT NULL, forename_user VARCHAR(30) NOT NULL, phone_user VARCHAR(10) NOT NULL, address_user VARCHAR(50) NOT NULL, city_user VARCHAR(30) NOT NULL, postal_code_user INT NOT NULL, car_model_user VARCHAR(30) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE annonce (id INT AUTO_INCREMENT NOT NULL, id_user_id INT DEFAULT NULL, nb_places INT NOT NULL, animaux_autorisees TINYINT(1) NOT NULL, fumeurs_autorises TINYINT(1) NOT NULL, adresse_depart VARCHAR(50) NOT NULL, ville_depart VARCHAR(30) NOT NULL, adresse_arrivee VARCHAR(50) NOT NULL, ville_arrivee VARCHAR(30) NOT NULL, date_heure_depart DATETIME NOT NULL, INDEX IDX_F65593E579F37AE5 (id_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE commenter (id INT AUTO_INCREMENT NOT NULL, id_annonce_id INT NOT NULL, id_user_id INT NOT NULL, commentaire VARCHAR(150) NOT NULL, note INT NOT NULL, INDEX IDX_AB751D0A2D8F2BF8 (id_annonce_id), INDEX IDX_AB751D0A79F37AE5 (id_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE reserver ADD CONSTRAINT FK_B9765E932D8F2BF8 FOREIGN KEY (id_annonce_id) REFERENCES annonce (id)');
        $this->addSql('ALTER TABLE reserver ADD CONSTRAINT FK_B9765E9379F37AE5 FOREIGN KEY (id_user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE annonce ADD CONSTRAINT FK_F65593E579F37AE5 FOREIGN KEY (id_user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE commenter ADD CONSTRAINT FK_AB751D0A2D8F2BF8 FOREIGN KEY (id_annonce_id) REFERENCES annonce (id)');
        $this->addSql('ALTER TABLE commenter ADD CONSTRAINT FK_AB751D0A79F37AE5 FOREIGN KEY (id_user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE reserver DROP FOREIGN KEY FK_B9765E9379F37AE5');
        $this->addSql('ALTER TABLE annonce DROP FOREIGN KEY FK_F65593E579F37AE5');
        $this->addSql('ALTER TABLE commenter DROP FOREIGN KEY FK_AB751D0A79F37AE5');
        $this->addSql('ALTER TABLE reserver DROP FOREIGN KEY FK_B9765E932D8F2BF8');
        $this->addSql('ALTER TABLE commenter DROP FOREIGN KEY FK_AB751D0A2D8F2BF8');
        $this->addSql('DROP TABLE reserver');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE annonce');
        $this->addSql('DROP TABLE commenter');
    }
}
